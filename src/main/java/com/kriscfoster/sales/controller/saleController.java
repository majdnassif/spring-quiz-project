package com.kriscfoster.sales.controller;

//import com.kriscfoster.sales.dao.OrderResponse;
import com.kriscfoster.sales.dao.OrderResponse;
import com.kriscfoster.sales.entity.Sale;
import com.kriscfoster.sales.entity.SellerProduct;
import com.kriscfoster.sales.service.saleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/sale")
@Validated
public class saleController {
    @Autowired
    public saleService saleservice;
    Logger logger = LoggerFactory.getLogger(sellerProductController.class);

    @PostMapping("/add")
    public Sale createSaleOperation(@Valid @RequestBody Sale sale){

        return saleservice.createSaleOperation(sale);

    }

    @PostMapping("/addMultipleSales")
    public List<Sale> createMultipleSales(@Valid @RequestBody List<Sale> sale){

        return saleservice.createMultipleSales(sale);

    }


    @GetMapping("/getAllSales")
    public List<OrderResponse>  getAllSalesInformation(){
        return saleservice.getAllSalesInformation();
    }

    @PutMapping("/edit")
    public Sale editSaleOperation(@RequestParam(value = "saleProductId") @NotNull(message="saleProductId musnt be null") Long saleProductId ,
                                            @RequestParam(value="price") double price,
                                            @RequestParam(value="quantity")  int quantity ){

        logger.debug("Entering update sale operation start point");
        logger.debug("start run in controller saleController-->editSaleOperation methode");

        return saleservice.editSaleOperation(saleProductId,price,quantity);
    }


}
