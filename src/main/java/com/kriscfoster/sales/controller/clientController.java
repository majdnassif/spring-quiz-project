package com.kriscfoster.sales.controller;

import com.kriscfoster.sales.entity.Client;
import com.kriscfoster.sales.entity.Product;
import com.kriscfoster.sales.service.clientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/client")
public class clientController {
    @Autowired
    private clientService clientservice;

    @PostMapping("/create")
    public Client createNewClient(@Valid @RequestBody Client client){
        //
        return clientservice.createClient(client);
    }

   @GetMapping("/fetchClient/{id}")
    public Client getProduct(@PathVariable Long id){
        return clientservice.getById((id));
    }
    @GetMapping("/fetchClients")
    public List<Client> getAllClients(){
        return clientservice.gellAllClient();
    }

    @PutMapping("/update")
    public ResponseEntity<Client> updateClient(@Valid @RequestBody Client client){


        Client result = clientservice.updateClient(client);
        return new ResponseEntity<>(result, HttpStatus.CREATED);

    }

}
