package com.kriscfoster.sales.controller;

import com.kriscfoster.sales.entity.Product;
import com.kriscfoster.sales.entity.SellerProduct;
import com.kriscfoster.sales.service.sellerProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/sellerProduct")
@Validated
public class sellerProductController {

    @Autowired
    private sellerProductService sellerProductservice;

    @PostMapping("/add")
       public SellerProduct createSellerProduct(@RequestParam(value="product_id")  Long product_id,
                                                @RequestParam(value="seller_id")  Long seller_id){
            return sellerProductservice.createSellerProduct(product_id,seller_id);
        }





}
