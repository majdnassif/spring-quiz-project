package com.kriscfoster.sales.controller;

import com.kriscfoster.sales.entity.Product;


import com.kriscfoster.sales.error.ConflictException;
import com.kriscfoster.sales.service.productService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")

public class productController {
    @Autowired
    private productService productService;

    @PostMapping("/create")
    public Product createNewProduct(@Valid @RequestBody Product product){
        //error handler dont send id
        return productService.createProduct(product);
    }

    @GetMapping("/fetchProduct/{id}")
    public Product getProduct(@PathVariable Long id){
        return productService.getById(id);
    }

    @GetMapping("/fetchProducts")
    public List<Product> getAllProducts(){
        return productService.gellAllProduct();
    }

   /* @PutMapping("/edit")
    public Product editProduct(@RequestBody Product product){
        return productService.productrepository.update(product);
    }*/

    @PutMapping("/update")

    public ResponseEntity<Product> updateProduct(@Valid @RequestBody Product product){

        Product result = productService.updateProduct(product);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
        //return ResponseEntity.ok(result);



    }


}
