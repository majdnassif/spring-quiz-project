package com.kriscfoster.sales.controller;

import com.kriscfoster.sales.entity.Product;

import com.kriscfoster.sales.entity.Seller;
import com.kriscfoster.sales.service.productService;
import com.kriscfoster.sales.service.sellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/seller")
@Validated
public class sellerController {
    @Autowired
    private sellerService sellerservice;

    @PostMapping("/create")
    public Seller createNewProduct(@RequestParam(value = "name")  @NotEmpty String name ){
        //error handler dont send id
        return sellerservice.createSeller(name);
    }
}
