package com.kriscfoster.sales.controller;


import com.kriscfoster.sales.entity.Category;
import com.kriscfoster.sales.error.ConflictException;
import com.kriscfoster.sales.service.categoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/category")
@Validated
public class categoryController {

    @Autowired
    categoryService categoryservice;

    @PostMapping("/add")
    public Category addCategory(@RequestParam(value = "categoryName")@NotNull(message = "category name cant be null") String categoryName){

        return categoryservice.add(categoryName);
    }

}
