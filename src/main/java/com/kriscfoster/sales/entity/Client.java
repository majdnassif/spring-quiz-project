package com.kriscfoster.sales.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Getter
@Setter
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

   @javax.validation.constraints.NotNull(message = "client name  is required")
    @javax.validation.constraints.NotEmpty(message = "client name must not be empty")
    private String name;
    private String lastName;
    private String mobile;

    @JsonIgnore
    @OneToMany(mappedBy = "client")
    private Set<Sale> sales;




}
