package com.kriscfoster.sales.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.Calendar;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Getter
@Setter
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    private double total; //equal ( price * quantity) in sellerProduct entity

    @javax.validation.constraints.NotNull(message = "product quantity  is required")
    @Min(value = 1,message = "minimum quantity is 1 ")
    private int quantity;
    @javax.validation.constraints.NotNull(message = "product price  is required")
    private double price; //price for one product


    @JsonIgnore
    @NotNull
    @Column(name = "create_time")
    private final Date createDate = Calendar.getInstance().getTime();


    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "sellerProduct_id", referencedColumnName = "id")
    private SellerProduct sellerProduct;

}
