package com.kriscfoster.sales.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity

public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @javax.validation.constraints.NotNull(message = "Category name  is required")
    @javax.validation.constraints.NotEmpty(message = "Category name must not be empty")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "category")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Set<Product> products;

}
