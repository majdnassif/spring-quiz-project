package com.kriscfoster.sales.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
      Long id;


    @javax.validation.constraints.NotNull(message = "product name  is required")
    @javax.validation.constraints.NotEmpty(message = "product name must not be empty")
    private String name;


    private String decription;


    @NotNull
    @Column(name = "create_time")
    private final Date createDate = Calendar.getInstance().getTime();

    @JsonIgnore
    @OneToMany(mappedBy = "product")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Set<SellerProduct> sellerproducts;



//   @JsonIgnore
    @ManyToOne  //(cascade = CascadeType.ALL) WHEN I delete this line then i added product correcty
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;


}
