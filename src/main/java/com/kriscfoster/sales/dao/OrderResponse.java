package com.kriscfoster.sales.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class OrderResponse {
    private  Long id;
    private String clientName;
    private String sellerName;
    private double total;
    private  Date createDate ;

    public OrderResponse( Long id  ,String clientName , String sellerName  , double total ,  Date createDate  ){
        this.clientName = clientName;
        this.id = id;
        this.total = total;
        this.sellerName = sellerName;
        this.createDate = createDate;

    }

}
