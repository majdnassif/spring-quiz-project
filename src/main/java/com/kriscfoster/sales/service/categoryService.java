package com.kriscfoster.sales.service;


import com.kriscfoster.sales.entity.Category;
import com.kriscfoster.sales.error.ConflictException;
import com.kriscfoster.sales.repository.categoryRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class categoryService {
    @Autowired
  public  categoryRepository categoryrepository;

   public Category add(String categoryName){
        if(categoryrepository.findByName(categoryName) !=null )
           throw new ConflictException("Another record with the same name exists");
        Category category =  new Category();
        category.setName(categoryName);
      return  categoryrepository.save(category);
    }


    public Boolean excist(Long id){
        if(categoryrepository.existsById(id))
            return true;

            return false;


    }

}
