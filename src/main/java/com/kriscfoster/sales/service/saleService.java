package com.kriscfoster.sales.service;

import com.kriscfoster.sales.controller.sellerProductController;
import com.kriscfoster.sales.dao.OrderResponse;
import com.kriscfoster.sales.entity.Client;
import com.kriscfoster.sales.entity.Sale;
import com.kriscfoster.sales.entity.SellerProduct;
import com.kriscfoster.sales.error.ConflictException;
import com.kriscfoster.sales.error.NotFoundException;
import com.kriscfoster.sales.repository.saleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class saleService {
    @Autowired
    public  saleRepository salerepository;

    @Autowired
   public  sellerProductService sellerproductservice;
    @Autowired clientService clientservice;

    Logger logger = LoggerFactory.getLogger(sellerProductController.class);

    public Sale createSaleOperation(Sale sale){
    if(sale.getClient().getId()!=null){
        if(sale.getSellerProduct().getId()!=null) {

                if(sellerproductservice.excist(sale.getSellerProduct().getId() ) ){

                    if(clientservice.excist(sale.getClient().getId() ) ){
                    /*
                      until this moment  (Sellerproduct and  client ) are empty object those have just id
                       so i will get all these object data to return it(inside sale object) after adding sale
                    */

                        SellerProduct Sellerproduct = sellerproductservice.getById(sale.getSellerProduct().getId());
                        Client client = clientservice.getById((sale.getClient().getId()));

                        sale.setTotal(sale.getPrice() * sale.getQuantity());
                        Sale saleAfterAdded =    salerepository.save(sale);

                        saleAfterAdded.setClient(client);
                        saleAfterAdded.setSellerProduct(Sellerproduct);
                        //to return sale after added  with new data
                        return saleAfterAdded;
                    }

                    throw new NotFoundException(String.format("No Record with the id [%s] was found in our database client", sale.getClient().getId()));

                }
                throw new NotFoundException(String.format("No Record with the id [%s] was found in our database sellerProduct", sale.getSellerProduct().getId() ));

      }
        throw new ConflictException("sellerProduct  id is required !!!");
    }
      throw new ConflictException("client id is required !!!");
    }


    public List<Sale> createMultipleSales(List<Sale> sales){
        ArrayList<Sale> sales_added   = new ArrayList<>();
        for (Sale  sale:sales){
            if(sale.getClient().getId()!=null){
            if(sale.getSellerProduct().getId()!=null) {

                if(sellerproductservice.excist(sale.getSellerProduct().getId() ) ){
                if(clientservice.excist(sale.getClient().getId() ) ){
                    SellerProduct Sellerproduct = sellerproductservice.getById(sale.getSellerProduct().getId());
                    Client client = clientservice.getById((sale.getClient().getId()));

                    sale.setTotal(sale.getPrice() * sale.getQuantity());
                    Sale saleAdded =     salerepository.save(sale);


                    saleAdded.setClient(client);
                    saleAdded.setSellerProduct(Sellerproduct);
                    sales_added.add(saleAdded);

                }else{
                    throw new NotFoundException(String.format("No Record with the id [%s] was found in our database client", sale.getClient().getId()));

                }


            }
                else{
                throw new NotFoundException(String.format("No Record with the id [%s] was found in our database sellerProduct", sale.getSellerProduct().getId() ));

            }

            }
            else{
                throw new ConflictException("sellerProduct  id is required !!!");
            }

        }
            else{
                throw new ConflictException("client id is required !!!");
            }
        }
        return sales_added;

    }



    public List<OrderResponse> getAllSalesInformation(){
        return salerepository.getAllSalesInformation();
    }

    public Sale editSaleOperation(Long saleProductId, double price, int quantity){

        logger.debug("start run in service saleService--> editSaleOperation method");

        logger.info("saleProductId is: "+saleProductId+"   price is: "+price+"   quantity is: "+quantity);
        try{
            if(quantity==0){
                logger.error("Ypu cant make  quantity equal 0 ");
                throw new ConflictException("Ypu cant make  quantity equal 0");
            }

            if(price==0.0){
                logger.warn("price is zero!! so you want to give this product as gift to client");
            }
            Sale sale = salerepository.findById(saleProductId).get();
            // logger.info("get sellerProduct that we want to update it : ", sellerproduct);


            logger.info("set price to new price: "+ price);
            sale.setPrice(price);
            logger.info("set quantity to new quantity: "+ quantity);
            sale.setQuantity(quantity);

            logger.info("set total to new total: "+ quantity*price);
            sale.setTotal(quantity*price);

            logger.info("updating the sellerProduct row with new  quantity and price");
            logger.debug("update sale operation endPoint");
            return salerepository.save(sale);

        }
        catch (NoSuchElementException ex) {
            logger.error("No Record with the id ["+saleProductId+ "]  was found in our sellerProduct database so you cant updating unexciting row");
            throw new NotFoundException(String.format("No Record with the id [%s] was found in our sellerProduct database", saleProductId));
        }


    }






}
