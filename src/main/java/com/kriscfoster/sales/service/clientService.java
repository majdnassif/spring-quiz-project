package com.kriscfoster.sales.service;

import com.kriscfoster.sales.entity.Client;
import com.kriscfoster.sales.entity.Product;
import com.kriscfoster.sales.error.ConflictException;
import com.kriscfoster.sales.error.NotFoundException;
import com.kriscfoster.sales.repository.clientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class clientService {
    @Autowired
    public clientRepository  clientrepository;


    public Boolean excist(Long id){
        if(clientrepository.existsById(id))
            return true;

        return false;


    }


    public Client createClient(Client client){
        /////////   reset id to forget  sented id then create client not update exciting client
        Long resetId = Long.valueOf(0);
        client.setId(resetId);
       //////////

        /*if(clientrepository.findByName(client.getName()) !=null)
            throw new ConflictException("this client name is already exist!! Please enter another name");*/
        return clientrepository.save(client);

    }




    public Client getById(Long id)
    {
        try {
            return  clientrepository.findById(id).get();
        }catch (NoSuchElementException ex) {
            throw new NotFoundException(String.format("No Record with the id [%s] was found in our database", id));
        }

    }
    public List<Client> gellAllClient(){
        return clientrepository.findAll();
    }
    public Boolean  existClient( Long id )
    {
        return  clientrepository.existsById(id);
    }



    public Client updateClient(Client newClient) {

        if( newClient.getId() != null){
            if (clientrepository.existsById(newClient.getId()))
            {// product witch that you wana update excist

                return clientrepository.save(newClient);
            }
            throw new NotFoundException(String.format("No Record with the id [%s] was found in our database", newClient.getId()));

        }
        throw new ConflictException("this client id is must not be null");

    }


}
