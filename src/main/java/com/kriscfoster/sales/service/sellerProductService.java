package com.kriscfoster.sales.service;

import com.kriscfoster.sales.controller.sellerProductController;
import com.kriscfoster.sales.entity.Product;
import com.kriscfoster.sales.entity.SellerProduct;
import com.kriscfoster.sales.error.ConflictException;
import com.kriscfoster.sales.error.NotFoundException;
import com.kriscfoster.sales.repository.sellerProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class sellerProductService {
    @Autowired
    public sellerProductRepository sellerProductrepository;
    @Autowired
    public productService productservice;

    @Autowired
    public sellerService sellerservice;

    public Boolean excist(Long id){
        if(sellerProductrepository.existsById(id))
            return true;

        return false;


    }




    public SellerProduct createSellerProduct( Long product_id,Long seller_id){
        if(productservice.excist(product_id) ){
            if(sellerservice.excist(seller_id) ){
                SellerProduct sellerProduct = new SellerProduct();
                sellerProduct.setSeller(sellerservice.getById(seller_id));
                sellerProduct.setProduct(productservice.getById(product_id));
                return sellerProductrepository.save(sellerProduct);


            }
            throw new NotFoundException(String.format("No Record with the id [%s] was found in our database seller", seller_id));


        }
        throw new NotFoundException(String.format("No Record with the id [%s] was found in our database product", product_id));

    }




    public SellerProduct getById(Long id)
    {

        try {

          //  SellerProduct p =  sellerProductrepository.findById(id).get();
            return  sellerProductrepository.findById(id).get();
        }catch (NoSuchElementException ex) {
            throw new NotFoundException(String.format("No Record with the id [%s] was found in our database", id));
        }



    }
}
