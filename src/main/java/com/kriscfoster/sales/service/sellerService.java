package com.kriscfoster.sales.service;

import com.kriscfoster.sales.entity.Product;
import com.kriscfoster.sales.entity.Seller;
import com.kriscfoster.sales.error.NotFoundException;
import com.kriscfoster.sales.repository.sellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class sellerService {
    @Autowired
    public sellerRepository sellerrepository;

    public Boolean excist(Long id){
        if(sellerrepository.existsById(id))
            return true;

        return false;


    }



    public Seller getById(Long id)
    {
        try {
             return  sellerrepository.findById(id).get();
        }catch (NoSuchElementException ex) {
            throw new NotFoundException(String.format("No Record with the id [%s] was found in our database", id));
        }



    }

    public Seller createSeller(String name){
        Seller seller = new Seller();
        seller.setName(name);
        return sellerrepository.save(seller);
    }
}
