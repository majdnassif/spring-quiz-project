package com.kriscfoster.sales.service;

import com.kriscfoster.sales.entity.Product;
import com.kriscfoster.sales.error.ConflictException;
import com.kriscfoster.sales.error.NotFoundException;
import com.kriscfoster.sales.repository.productRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Service
public class productService {
    @Autowired
    public productRepository productrepository;


    @Autowired
    public categoryService categoryservice;



    public Boolean excist(Long id){
        if(productrepository.existsById(id))
            return true;
        return false;
    }


   public Product getById(Long id)
    {
        try {

            return  productrepository.findById(id).get();
        }catch (NoSuchElementException ex) {
            throw new NotFoundException(String.format("No Record with the id [%s] was found in our database", id));
        }



    }
    public List<Product> gellAllProduct(){
       return productrepository.findAll();
    }

    public Product createProduct(Product product ){

          if(product.getCategory().getId()!=null){
              if(categoryservice.excist(product.getCategory().getId() ) ){
                  //category for this product excist so go on

                  /////////   reset id to forget  sented id then create product not update exciting product
                  Long resetId = Long.valueOf(0);
                  product.setId(resetId);


                  if(productrepository.findByName(product.getName()) !=null)
                      throw new ConflictException("this product name is already exist!! Please enter another name");
                  return productrepository.save(product);
              }
              throw new NotFoundException(String.format("No Record with the id [%s] was found in our database category", product.getCategory().getId()));

          }
        throw new ConflictException("you can't add product without category id!! so add category id");


    }


    public Product updateProduct(Product newProduct) {

        if(newProduct.getCategory().getId()!=null){
        if(categoryservice.excist(newProduct.getCategory().getId() ) ){
            if (newProduct.getId() != null) {
                if (productrepository.existsById(newProduct.getId())) {// product witch that you wana update excist

                    if (productrepository.findByName(newProduct.getName()) == null //new name
                            ||
                            productrepository.findByName(newProduct.getName()).getId() == newProduct.getId() //new name equal old name for this product

                    ) {
                        //i will update product with new name
                        return productrepository.save(newProduct);
                    } else { //the new name of product has been excisted in db already

                        throw new ConflictException("this product name is already exist!! Please enter another name");

                    }
                }
                throw new NotFoundException(String.format("No Record with the id [%s] was found in our database", newProduct.getId()));

            }
            throw new ConflictException("this product id is must not be null");
        }
        throw new NotFoundException(String.format("No Record with the id [%s] was found in our database category", newProduct.getCategory().getId()));
        }
        throw new ConflictException("you can't add product without category id!! so add category id");



    }


}
