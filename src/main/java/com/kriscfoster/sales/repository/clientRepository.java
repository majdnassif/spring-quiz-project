package com.kriscfoster.sales.repository;

import com.kriscfoster.sales.entity.Client;
import com.kriscfoster.sales.entity.Product;
import com.kriscfoster.sales.entity.SellerProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface clientRepository  extends JpaRepository<Client,Long> {
    Client findByName(String name);

}
