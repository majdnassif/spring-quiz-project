package com.kriscfoster.sales.repository;

import com.kriscfoster.sales.entity.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface sellerRepository extends JpaRepository<Seller,Long> {
}
