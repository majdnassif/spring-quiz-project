package com.kriscfoster.sales.repository;


import com.kriscfoster.sales.dao.OrderResponse;
import com.kriscfoster.sales.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface categoryRepository  extends JpaRepository<Category, Long> {

/*
    @Query("SELECT new com.kriscfoster.sales.dao.OrderResponse(c.name ) FROM Category c JOIN c.products p")
    public List<OrderResponse> getJoinInformation();*/


    // as query
    Category findByName(String name);
}
