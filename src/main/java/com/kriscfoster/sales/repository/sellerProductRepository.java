package com.kriscfoster.sales.repository;

import com.kriscfoster.sales.entity.Seller;
import com.kriscfoster.sales.entity.SellerProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface sellerProductRepository extends JpaRepository<SellerProduct,Long> {
}
