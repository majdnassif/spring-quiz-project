package com.kriscfoster.sales.repository;

import com.kriscfoster.sales.dao.OrderResponse;
import com.kriscfoster.sales.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface saleRepository extends JpaRepository<Sale,Long> {

    @Query("SELECT new com.kriscfoster.sales.dao.OrderResponse(s.id ,c.name ,sp.seller.name  ,s.total ,s.createDate) FROM Sale s JOIN s.client c JOIN s.sellerProduct sp")
    public List<OrderResponse> getAllSalesInformation();

}
