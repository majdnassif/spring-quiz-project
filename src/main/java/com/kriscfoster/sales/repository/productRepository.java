package com.kriscfoster.sales.repository;

import com.kriscfoster.sales.dao.OrderResponse;
import com.kriscfoster.sales.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface productRepository  extends JpaRepository<Product,Long> {
Product findByName(String name);

  /*  @Query("SELECT new com.javatechie.jpa.dto.OrderResponse(c.name , p.productName) FROM Customer c JOIN c.products p")
    public List<OrderResponse> update();*/


}
